class Storage {
	constructor() {
		;
	}

	reset(preserve) {
		;
	}

	data(key) {
		;
	}
}
let storage = new Storage();

class Gateway {
	auth() {
		return fetch('/gateway', { method: 'POST', credentials: 'same-origin' }).then(response => {
			return response.text().then(text => {
				let data = JSON.parse(text);
				if(data.error) { throw data.error; }
			});
		});
	}
}
let gateway = new Gateway();

let AdminPage = Vue.extend({
	template: document.getElementById('admin-page-template').innerHTML,
	data: () => ({
		tab: 'dashboard',
		meters: [
			{name: 'Storage', max: 10, value: 7},
			{name: 'Bandwidth', max: 10, value: 2},
		],
		apps: [
			{name: 'Cool App', description: 'It\'s so cool dude', remove: false, removing: false},
			{name: 'Lame App', description: 'It\'s so lame dude', remove: false, removing: false},
		],
		services: [
			{name: 'Cool Service', description: 'It\'s so cool dude'},
			{name: 'Lame Service', description: 'It\'s so lame dude'},
		],
	}),
	methods: {
		removeApp(app) {
			app.removing = true;
			setTimeout(() => {
				app.removing = false;
				app.remove = false;
			}, 2000);
		}
	},
});

let LoginPage = Vue.extend({
	template: document.getElementById('login-page-template').innerHTML,
	data: () => ({
		valid: false,
		show_passphrase: false,
		passphrase: '',
		passphrase_rules: [value => !!value || 'Please enter your passphrase'],
		preserve_session: false,
		logging_in: false,
		show_error: false,
		error: '',
	}),
	methods: {
		submit() {
			this.logging_in = true;
			fetch('/gateway/login'+(this.preserve_session ? '?persist' : ''), {
				method: 'POST',
				headers: {Authorization: 'Basic '+window.btoa(':'+this.passphrase)},
				credentials: 'same-origin',
			}).then(response => {
				this.logging_in = false;
				if(response.status === 401) {
					this.error = 'Incorrect passphrase.';
					this.show_error = true;
				} else {
					gateway.auth().then(() => {
						router.push({ path:'/' });
					}).catch(error => {
						this.logging_in = false;
						this.error = error.message;
						this.show_error = true;
					});
				}
			}, error => {
				this.logging_in = false;
				this.error = error.message;
				this.show_error = true;
			});
		}
	},
});

let AuthPage = Vue.extend({
	template: document.getElementById('auth-page-template').innerHTML,
});

let router = new VueRouter({
	mode: 'history',
	routes: [
		{ path: '/login', component: LoginPage },
		{ path: '/auth', component: AuthPage },
		{ path: '/', component: AdminPage },
	],
});

let app = new Vue({
	router,
	el: '#root',
});
