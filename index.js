const express = require('express');
const fs = require('fs-extra');
const bcrypt = require('bcrypt-nodejs');
const rsa = require('node-rsa');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

function load(name) {
	return fs.exists(`${__dirname}/.data/${name}`).then(exists => exists ? fs.readFile(`${__dirname}/.data/keys`).then(data => JSON.parse(data)) : null);
}

function save(name, data) {
	return fs.ensureDir(`${__dirname}/.data`).then(() => fs.writeFile(`${__dirname}/.data/${name}`, JSON.stringify(data)));
}

function generateKeys() {
	let key = new rsa();
	key.generateKeyPair();
	public_key = key.exportKey('pkcs1-public-pem');
	private_key = key.exportKey('pkcs1-private-pem');
	return save('keys', {public_key, private_key});
}

function sign(audience, scope, options) {
	options = options || {};
	return jwt.sign({iss: gateway_endpoint, aud: audience, scope: scope}, private_key, {algorithm: 'RS512', expiresIn: options.duration || '8h'});
}

function verify(token, audience, scope) {
	return new Promise((resolve, reject) => {
		jwt.verify(token, public_key, {algorithm: 'RS512', issuer: gateway_endpoint, audience}, (error, decoded) => {
			if(error) { return reject(error); }
			if(!_.isString(decoded.scope)) { return reject(new Error('Invalid token scope')); }
			let scopes = decoded.scope.split(' ');
			if(scopes.indexOf(scope) === -1) { return reject(new Error(`Required scope ${scope} not provided`)); }
			resolve(decoded);
		});
	});
}

function verify_request(audience, scope, verified) {
	return (req, res, next) => {
		let reject = () => { res.status(401); res.end(); };
		if(!req.headers.authorization || !req.headers.authorization.includes('Bearer ')) { return reject(); }
		verify(req.headers.authorization.replace('Bearer ', ''), audience, scope).then(() => verified(req, res, next), reject);
	};
}

const passphrase_hash = '$2a$10$QALtNOgL09js.Hzp9wkd0.VzFwdFJag/cS6GNHLVGubV9BkVkH/Ky';
const gateway_port = 8080;
const gateway_hostname = '127.0.0.1';
const gateway_protocol = 'http://';
const gateway_domain = gateway_hostname + (gateway_port === 80 ? '' : ':'+gateway_port.toString());
const gateway_path = '/gateway';
const gateway_endpoint = gateway_protocol+gateway_domain+gateway_path;
const allowed_paths = [gateway_protocol+gateway_domain, gateway_protocol+gateway_domain+'/auth', gateway_protocol+gateway_domain+'/login'];
let public_key;
let private_key;

const app = express();
app.use(require('cookie-parser')());

app.post('/gateway/login', (req, res) => {
	if(req.headers.authorization && req.headers.authorization.includes('Basic ')) {
		let credentials = Buffer.from(req.headers.authorization.replace('Basic ', ''), 'base64').toString('utf-8');
		let duration = req.query.persist ? 365*24*60*60*1000 : 8*60*60*1000;

		bcrypt.compare(credentials.length ? credentials.slice(1) : '', passphrase_hash, (error, valid) => {
			if(valid === true) {
				let login = sign(gateway_endpoint, 'login', {duration});
				res.status(200);
				res.cookie('login', login, { maxAge: duration, httpOnly: true, domain: gateway_hostname, path: gateway_path }); //TODO: add secure flag when self-signed ssl is implemented
				res.end();
			} else {
				res.status(401);
				res.set('WWW-Authenticate', `Basic realm="${gateway_endpoint}"`);
				res.send('Invalid credentials');
			}
		});
	} else {
		res.status(401);
		res.set('WWW-Authenticate', `Basic realm="${gateway_endpoint}"`);
		res.end();
	}
});

app.get('/gateway', (req, res) => res.send(public_key));

app.options('/gateway', (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.sendStatus(200);
});

app.post('/gateway', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	res.send(JSON.stringify(req.headers));
	return;
	if(!req.headers.referer) {
		return res.send(JSON.stringify({error: {message: `Invalid request: Missing referrer header`, code: 'INVALID_REQUEST'}}));
	}
	if(!req.headers.origin) {
		return res.send(JSON.stringify({error: {message: `Invalid request: Missing origin header`, code: 'INVALID_REQUEST'}}));
	}
	if(!_.startsWith(req.headers.referer, req.headers.origin)) {
		return res.send(JSON.stringify({error: {message: `Invalid request: Referrer and origin do not match`, code: 'INVALID_REQUEST'}}));
	}
	if(!_.includes(allowed_paths, req.headers.referer)) {
		return res.send(JSON.stringify({error: {message: `Invalid application path "${req.headers.referer}". Allowed paths include: ${allowed_paths.join(', ')}`, code: 'INVALID_REQUEST'}}));
	}
	verify(req.cookies.login, gateway_endpoint, 'login').then(() => {
		res.send({gateway: sign(gateway_endpoint, 'all')});
	}, error => {
		res.send(JSON.stringify({error: {message: error.message, code: 'NOT_AUTHENTICATED'}}));
	});
});

app.get('/gateway/limits', verify_request(gateway_endpoint, 'all', (req, res) => res.send({
	storage: {current: 7, limit: 10},
	bandwidth: {current: 2, limit: 10},
})));

app.use(express.static('static'));
app.use((req, res, next) => req.url.includes('.') ? next() : res.sendFile(`${__dirname}/static/index.html`));

load('keys').then(keys => keys ? ({public_key, private_key} = keys) : generateKeys())
.then(() => app.listen(gateway_port, gateway_hostname), error => { console.error(error); process.exit(); });
